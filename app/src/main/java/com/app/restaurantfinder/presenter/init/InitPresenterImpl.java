package com.app.restaurantfinder.presenter.init;


import android.content.Context;
import android.location.Location;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.db.DatabaseApi;
import com.app.restaurantfinder.entity.restaurant.LocationModel;
import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;
import com.app.restaurantfinder.mapper.DataMapper;
import com.app.restaurantfinder.model.init.ResponseModel;
import com.app.restaurantfinder.model.init.RestaurantResponseModel;
import com.app.restaurantfinder.util.SharedPreferenceUtils;
import com.app.restaurantfinder.view.activity.init.InitView;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class InitPresenterImpl implements InitPresenter {
    private final Context context;
    private final InitView view;
    public final static int MAX_SIZE = 60;
    @Override
    public void addData(Location location, String json) {
        Gson gson = new Gson();
        Random random = new Random();
        DatabaseApi databaseApi = DatabaseApi.getInstance();
        ResponseModel responseModel = gson.fromJson(json, ResponseModel.class);
        Set<RestaurantResponseModel> restaurants = responseModel.getRestaurants();
        List<RestaurantResponseModel> models = new ArrayList<>(restaurants);
        DataMapper mapper = new DataMapper();
        if (MAX_SIZE < restaurants.size()) {
            for (int i = 0; i < MAX_SIZE; i++) {
                RestaurantResponseModel restaurantResponseModel = models.get(i);
                String imgPath = context.getExternalFilesDir("covers").getAbsolutePath() + File.separator + (i + 1) + ".jpg";

                double yDistance;
                double xDistance;
                boolean xPlus;
                boolean yPlus;
                if (i % 2 == 0) {
                    if (i <= MAX_SIZE/2) {
                        xDistance = random.nextDouble() * (0.09 - 0.01) + 0.01;
                        yDistance = random.nextDouble() * (0.09 - 0.01) + 0.01;

                    } else {
                        xDistance = random.nextDouble() * (0.09 - 0.01) + 0.1;
                        yDistance = random.nextDouble() * (0.09 - 0.01) + 0.1;
                    }

                } else {
                    if (i <= MAX_SIZE/2) {
                        xDistance = random.nextDouble() * (0.09 - 0.01) + 0.01;
                        yDistance = random.nextDouble() * (0.09 - 0.01) + 0.01;
                    } else {
                        xDistance = random.nextDouble() * (0.09 - 0.01) + 0.1;
                        yDistance = random.nextDouble() * (0.09 - 0.01) + 0.1;
                    }
                }
                if (i % 2 == 0) {
                    if (i % 4 == 0) {
                        yPlus = true;
                    } else {
                        yPlus = false;
                    }
                    xPlus = true;
                } else {
                    if (i % 3 == 0) {
                        yPlus = true;
                    } else {
                        yPlus = false;
                    }
                    xPlus = false;
                }
                LocationModel newLocation = getLocation(location, xDistance, yDistance, xPlus, yPlus);
                RestaurantEntity map = mapper.map(restaurantResponseModel, imgPath, newLocation);
                if (i == MAX_SIZE- 1) {
                    databaseApi.insertRestaurant(map).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(this::onSuccess, this::onError);
                } else {
                    databaseApi.insertRestaurant(map).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(this::dataAdded, this::onError);
                }
            }
        }else {
            throw new RuntimeException("Invalid Size");
        }

    }


    private void onSuccess() {
        SharedPreferenceUtils.databaseCreated(context);
        view.onSuccess();
    }

    private void onError(Throwable throwable) {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace();
        }
    }

    private void dataAdded() {
        view.onProgress();
    }

    private LocationModel getLocation(Location currentLocation, double xDistance, double yDistance, boolean xPlus, boolean yPlus) {
        LocationModel locationModel = new LocationModel();
        if (yPlus) {
            locationModel.setLatitude(currentLocation.getLatitude() + yDistance);
        } else {
            locationModel.setLatitude(currentLocation.getLatitude() - yDistance);
        }

        if (xPlus) {
            locationModel.setLongitude(currentLocation.getLongitude() + xDistance);
        } else {
            locationModel.setLongitude(currentLocation.getLongitude() - xDistance);
        }
        return locationModel;
    }

}
