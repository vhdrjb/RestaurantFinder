package com.app.restaurantfinder.presenter.restaurantFood;

import com.app.restaurantfinder.model.enums.RestaurantStyle;

public interface FoodsPresenter {
    void getFoods(RestaurantStyle style);
}
