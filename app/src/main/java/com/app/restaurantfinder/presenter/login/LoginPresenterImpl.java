package com.app.restaurantfinder.presenter.login;

import android.content.Context;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.util.SharedPreferenceUtils;
import com.app.restaurantfinder.view.activity.login.LoginView;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LoginPresenterImpl implements LoginPresenter {
    private final Context context;
    private final LoginView view;
    @Override
    public void authenticate(String mobile, String password) {
        boolean registeredUser = SharedPreferenceUtils.login(context, mobile, password);
        if (registeredUser) {
            view.onSuccess();
        } else {
            view.onFailure(R.string.authentication_failed_message);
        }
    }
}
