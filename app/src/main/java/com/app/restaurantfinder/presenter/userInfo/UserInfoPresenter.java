package com.app.restaurantfinder.presenter.userInfo;

import com.app.restaurantfinder.entity.user.UserEntity;

public interface UserInfoPresenter {
    void getUserInfo();

    void logOut();
}
