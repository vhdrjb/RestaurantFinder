package com.app.restaurantfinder.presenter.init;


import android.location.Location;

public interface InitPresenter {
    void addData(Location location, String json);
}
