package com.app.restaurantfinder.presenter.main;

import android.database.Observable;

import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;

public interface RestaurantPresenter {
   void findAll();
}
