package com.app.restaurantfinder.presenter.restaurantFood;

import android.content.Context;

import com.app.restaurantfinder.entity.food.FoodEntity;
import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.app.restaurantfinder.model.init.FoodResponse;
import com.app.restaurantfinder.view.activity.restaurantMenu.RestaurantView;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FoodsPresenterImpl implements FoodsPresenter {
    private final Context context;
    private final RestaurantView view;

    @Override
    public void getFoods(RestaurantStyle style) {
        String s = loadJSONFromAsset(style);
        Gson gson = new Gson();
        FoodResponse foodResponse = gson.fromJson(s, FoodResponse.class);
        view.onFoods(foodResponse);
    }

    public String loadJSONFromAsset(RestaurantStyle style) {
        String json = null;
        String jsonPath = null;
        if (style.equals(RestaurantStyle.RESTAURANT)) {
            jsonPath = "data/restaurant_response.json";
        } else if (style.equals(RestaurantStyle.SANDWICH)) {
            jsonPath = "data/sandwich_response.json";
        } else if (style.equals(RestaurantStyle.PIZZA)) {
            jsonPath = "data/pizza_response.json";
        } else if (style.equals(RestaurantStyle.NATIONAL_FOOD)) {
            jsonPath = "data/national_response.json";
        } else if (style.equals(RestaurantStyle.CAFE)) {
            jsonPath = "data/cafe_response.json";
        } else if (style.equals(RestaurantStyle.FRIED)) {
            jsonPath = "data/fried_response.json";
        } else {
            jsonPath = "data/restaurant_response.json";
        }
        try {
            InputStream is = context.getAssets().open(jsonPath);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
