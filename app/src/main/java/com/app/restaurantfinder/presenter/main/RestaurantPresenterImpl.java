package com.app.restaurantfinder.presenter.main;

import android.annotation.SuppressLint;
import android.content.Context;

import com.app.restaurantfinder.db.DatabaseApi;
import com.app.restaurantfinder.entity.restaurant.LocationModel;
import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;
import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.app.restaurantfinder.model.map.RestaurantModel;
import com.app.restaurantfinder.view.activity.main.RestaurantView;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RestaurantPresenterImpl implements RestaurantPresenter {
    private final Context context;
    private final RestaurantView restaurantView;
    private Disposable disposable;
    @Override
    public void findAll() {
        DatabaseApi databaseApi = DatabaseApi.getInstance();
        disposable = databaseApi.findAllRestaurant().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::onRestaurantFind, this::onError);
    }

    private void onRestaurantFind(List<RestaurantEntity> restaurantEntities) {
        for (RestaurantEntity restaurantEntity : restaurantEntities) {
            onView(restaurantEntity);
        }
    }

    private void onError(Throwable throwable) {

    }

    private void onView(final RestaurantEntity restaurantEntity) {
        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setType(restaurantEntity.getType());
        restaurantModel.setTitle(restaurantEntity.getRestaurantName());
        restaurantModel.setRate(restaurantEntity.getRate());
        restaurantModel.setLocation(new LatLng(restaurantEntity.getLocationModel().getLatitude(),
                restaurantEntity.getLocationModel().getLongitude()));
        restaurantModel.setImg(restaurantEntity.getImg());
        restaurantView.onRestaurantFind(restaurantModel);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void cancel() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.isDisposed();
        }
    }
}
