package com.app.restaurantfinder.presenter.userInfo;

import android.content.Context;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.db.DatabaseApi;
import com.app.restaurantfinder.entity.user.UserEntity;
import com.app.restaurantfinder.util.SharedPreferenceUtils;
import com.app.restaurantfinder.view.activity.main.UserInfoView;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserInfoPresenterImpl implements UserInfoPresenter {
    private final Context context;
    private final UserInfoView view;
    private Disposable disposable;

    @Override
    public void getUserInfo() {
        DatabaseApi databaseApi = DatabaseApi.getInstance();
        boolean registeredUser = SharedPreferenceUtils.isRegisteredUser(context);
        if (registeredUser) {
            view.onWaitUserData();
            long customerDbKey = SharedPreferenceUtils.getCustomerDbKey(context);
            disposable = databaseApi.findById(customerDbKey).
                    observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::onUserData, this::onUserError);

        }
    }

    @Override
    public void logOut() {
        SharedPreferenceUtils.logOutUser(context);
        view.onLogOut();
    }

    private void onUserError(Throwable throwable) {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace();
        }
        view.redirectToLogin();
    }

    private void onUserData(UserEntity userEntity) {
        view.onUserData(userEntity);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void cancel() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.isDisposed();
        }
    }
}
