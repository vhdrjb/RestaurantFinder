package com.app.restaurantfinder.presenter.register;

import android.content.Context;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.R;
import com.app.restaurantfinder.db.DatabaseApi;
import com.app.restaurantfinder.entity.user.UserEntity;
import com.app.restaurantfinder.util.SharedPreferenceUtils;
import com.app.restaurantfinder.view.activity.register.RegisterView;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RegisterPresenterImpl implements RegisterPresenter {
    private final Context context;
    private final RegisterView view;
    private Disposable disposable;

    @Override
    public void register(UserEntity userModel, String password) {
        DatabaseApi databaseApi = DatabaseApi.getInstance();
        disposable = databaseApi.insertUser(userModel).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(id -> onRegister(id, userModel.getPhoneNumber(), password), this::onFailure);

    }

    private void onFailure(Throwable throwable) {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace();
        }
        view.onFailure(R.string.register_failure_message);
    }

    private void onRegister(Long id, String mobile, String password) {
        SharedPreferenceUtils.registerUser(context, id, mobile, password);
        view.onRegister();
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void cancel() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.isDisposed();
        }
    }
}
