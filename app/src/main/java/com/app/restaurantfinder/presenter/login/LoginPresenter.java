package com.app.restaurantfinder.presenter.login;

public interface LoginPresenter {
    void authenticate(String mobile, String password);
}
