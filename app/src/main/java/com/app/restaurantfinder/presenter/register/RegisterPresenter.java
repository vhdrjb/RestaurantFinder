package com.app.restaurantfinder.presenter.register;

import com.app.restaurantfinder.entity.user.UserEntity;

public interface RegisterPresenter {
    void register(UserEntity userModel, String password);
}
