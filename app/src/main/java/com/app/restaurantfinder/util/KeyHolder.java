package com.app.restaurantfinder.util;

public final class KeyHolder {
    public final static String RESTAURANT_LIST_KEY = "restaurantsList";
    public final static String RESTAURANT_LOCATION_KEY = "restaurantLocation";
    public final static String RESTAURANT_VIEW_KEY = "restaurantView";
    public final static String RESTAURANT_FOOD_KEY = "restaurantFoodKey";
    public final static String RESTAURANT_FOOD_CATEGORY_KEY = "restaurantCategoryFoodKey";
}
