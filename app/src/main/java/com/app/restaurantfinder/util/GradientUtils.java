package com.app.restaurantfinder.util;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.app.restaurantfinder.model.map.FoodModel;

public class GradientUtils {
    public   static int getBackground(FoodModel foodModel) {
       if (foodModel.getType().equals(RestaurantStyle.SANDWICH)) {
            return R.drawable.sandwich_back_menu_drawable;
        } else if (foodModel.getType().equals(RestaurantStyle.PIZZA)) {
            return R.drawable.pizza_back_menu_drawable;
        } else if (foodModel.getType().equals(RestaurantStyle.NATIONAL_FOOD)) {
            return R.drawable.national_food_menu_back_drawable;
        } else if (foodModel.getType().equals(RestaurantStyle.RESTAURANT)) {
            return R.drawable.restaurant_menu_back_drawable;
        } else if (foodModel.getType().equals(RestaurantStyle.CAFE)) {
            return R.drawable.cafe_menu_back_drawable;
        } else if (foodModel.getType().equals(RestaurantStyle.ALL)) {
            return R.drawable.all_back_menu_drawable;
        } else {
            return R.drawable.fried_menu_back_drawable;
        }
    }



}
