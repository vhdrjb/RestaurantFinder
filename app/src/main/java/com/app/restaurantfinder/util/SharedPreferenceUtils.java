package com.app.restaurantfinder.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.restaurantfinder.BuildConfig;

import org.apache.commons.lang3.StringUtils;

public class SharedPreferenceUtils {
    private final static String PREFERENCE_NAME = BuildConfig.APPLICATION_ID + "_android_app";
    private final static String MOBILE_KEY = "user_mobile_key";
    private final static String PASSWORD_KEY = "user_password_key";
    private final static String DB_KEY = "user_database_key";
    private final static String DATABASE_CREATE_KEY = "database_create_key";
    private final static String LOGIN_KEY = "user_login_key";

    private static SharedPreferences getSharedPreference(Context context) {
        return context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public static boolean login
            (Context context, String mobile, String password) {


        String mobileValue = getSharedPreference(context)
                .getString(MOBILE_KEY, null);
        if (StringUtils.isEmpty(mobileValue) && StringUtils.equalsAnyIgnoreCase(mobile, mobileValue)) {
            return false;
        } else {
            String passwordValue = getSharedPreference(context)
                    .getString(PASSWORD_KEY, null);

            boolean loginResult = StringUtils.isNotEmpty(passwordValue) &&
                    StringUtils.equalsAnyIgnoreCase(passwordValue, password);
            getSharedPreference(context).edit().putBoolean(LOGIN_KEY, loginResult).apply();
            return loginResult;

        }
    }


    public static boolean isRegisteredUser(Context context) {
        return getSharedPreference(context).getBoolean(LOGIN_KEY, false);
    }

    public static void logOutUser(Context context) {
        getSharedPreference(context).edit()
                .putBoolean(LOGIN_KEY, false)
                .apply();

    }

    public static void deleteUser(Context context) {
        getSharedPreference(context)
                .edit()
                .remove(DB_KEY)
                .remove(MOBILE_KEY)
                .remove(PASSWORD_KEY)
                .remove(DATABASE_CREATE_KEY)
                .apply();

    }

    public static long getCustomerDbKey(Context context) {
        return getSharedPreference(context).getLong(DB_KEY, -1);
    }

    public static void databaseCreated(Context context) {
        getSharedPreference(context).edit().putBoolean(DATABASE_CREATE_KEY, true).apply();
    }

    public static boolean isDatabaseCreated(Context context) {
        return getSharedPreference(context).getBoolean(DATABASE_CREATE_KEY, false);
    }

    public static void registerUser(Context context, long key, String mobile, String password) {
        getSharedPreference(context)
                .edit()
                .putLong(DB_KEY, key)
                .putString(MOBILE_KEY, mobile)
                .putString(PASSWORD_KEY, password)
                .apply();
    }
}
