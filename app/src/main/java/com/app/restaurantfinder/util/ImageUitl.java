package com.app.restaurantfinder.util;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.app.restaurantfinder.model.map.FoodModel;

public class ImageUitl {
    public static int getImageResource(FoodModel markerModel) {
        if (markerModel.getType().equals(RestaurantStyle.SANDWICH)) {
            return R.drawable.ic_sandwich;
        } else if (markerModel.getType().equals(RestaurantStyle.PIZZA)) {
            return R.drawable.ic_pizza;
        } else if (markerModel.getType().equals(RestaurantStyle.NATIONAL_FOOD)) {
            return R.drawable.ic_national_food;
        } else if (markerModel.getType().equals(RestaurantStyle.RESTAURANT)) {
            return R.drawable.ic_restaurant;
        } else if (markerModel.getType().equals(RestaurantStyle.CAFE)) {
            return R.drawable.ic_cafe;
        } else if (markerModel.getType().equals(RestaurantStyle.ALL)) {
            return R.drawable.ic_all;
        } else {
            return R.drawable.ic_fried;
        }
    }
}
