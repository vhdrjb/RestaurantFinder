package com.app.restaurantfinder.util;

import java.text.NumberFormat;

public class CurrencyUtils {
    public static String makeMoney(String money) {
        String replaceable = String.format("[%s,.\\s]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
        String cleanString = money.replaceAll(replaceable, "");

        double parsed;
        try {
            parsed = Double.parseDouble(cleanString);
        } catch (NumberFormatException e) {
            parsed = 0.00;
        }
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        formatter.setMaximumFractionDigits(0);
        return formatter.format((parsed)).replaceAll("\\$", "");
    }

    public static String makeMoney(int money) {
        return makeMoney(String.valueOf(money));
    }
}
