package com.app.restaurantfinder.util;

import android.app.Application;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.db.DatabaseApi;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppConfig extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        DatabaseApi.init(getApplicationContext());

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
