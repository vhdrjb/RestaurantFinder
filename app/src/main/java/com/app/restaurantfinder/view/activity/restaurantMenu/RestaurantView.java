package com.app.restaurantfinder.view.activity.restaurantMenu;

import com.app.restaurantfinder.entity.food.FoodEntity;
import com.app.restaurantfinder.model.init.FoodResponse;

import java.util.List;

public interface RestaurantView {
    void onFoods(FoodResponse foodEntities);
}
