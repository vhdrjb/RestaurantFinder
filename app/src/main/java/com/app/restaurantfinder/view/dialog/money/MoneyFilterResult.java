package com.app.restaurantfinder.view.dialog.money;

public interface MoneyFilterResult {

    void onMoneyFilter(int start, int end);

    void removeFilter();
}
