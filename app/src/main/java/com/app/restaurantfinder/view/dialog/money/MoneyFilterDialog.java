package com.app.restaurantfinder.view.dialog.money;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.util.CurrencyUtils;

import org.jetbrains.annotations.NotNull;

import lombok.RequiredArgsConstructor;
import me.bendik.simplerangeview.SimpleRangeView;

@RequiredArgsConstructor
public class MoneyFilterDialog {
    private final Context context;
    private final MoneyFilterResult result;
    private final static int MAX_MONEY = 200;
    private int startMoneyValue = 0;
    private int endMoneyValue = MAX_MONEY*1000;
    private AlertDialog alertDialog;
    private void init() {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_money_filter, null, false);
        final SimpleRangeView rangeView = view.findViewById(R.id.range);
        final TextView startMoney = view.findViewById(R.id.startMoney);
        final TextView endMoney = view.findViewById(R.id.endMoney);

        startMoney.setText(CurrencyUtils.makeMoney(startMoneyValue));
        endMoney.setText(CurrencyUtils.makeMoney(endMoneyValue));

        rangeView.setCount(MAX_MONEY);
        rangeView.setStart(0);
        rangeView.setEnd(endMoneyValue / 1000);
        rangeView.setOnChangeRangeListener((simpleRangeView, i, i1) -> {
            startMoneyValue = i*1000;
            endMoneyValue = i1*1000;
        });
        rangeView.setOnTrackRangeListener(new SimpleRangeView.OnTrackRangeListener() {
            @Override
            public void onStartRangeChanged(@NotNull SimpleRangeView simpleRangeView, int i) {
                startMoney.setText(CurrencyUtils.makeMoney(i*1000));
            }
            @Override
            public void onEndRangeChanged(@NotNull SimpleRangeView simpleRangeView, int i) {
                endMoney.setText(CurrencyUtils.makeMoney(i*1000));
            }
        });
         alertDialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(false)
                .setPositiveButton(R.string.money_apply, (dialog, which) -> {
                    dialog.dismiss();
                    result.onMoneyFilter(startMoneyValue, endMoneyValue);
                })
                .setNegativeButton(R.string.money_cancel,(dialog, which) -> dialog.cancel())
                 .setNeutralButton(R.string.disable_filter,(dialog, which) -> {
                     result.removeFilter();
                     dialog.dismiss();
                 })
                .create();
    }
    public void show() {
        if (alertDialog == null) {
            init();
        }
        alertDialog.show();
    }

}
