package com.app.restaurantfinder.view.activity.init;

public interface InitView {
    void onSuccess();

    void onProgress();
}
