package com.app.restaurantfinder.view.dialog.rate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.R;
import com.app.restaurantfinder.db.DatabaseApi;
import com.app.restaurantfinder.model.map.RestaurantModel;

import androidx.appcompat.app.AlertDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RatingDialog {
    private final Context context;
    private final RestaurantModel restaurantEntity;
    private final RateResult result;
    private float rate;

    public void show() {
        rate = restaurantEntity.getRate();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_rating, null, false);
        final RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener((ratingBar1, rating, fromUser) -> {
            rate = rating;
            ratingBar1.setRating(rate);
        });
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setView(view)
                .setCancelable(false)
                .setPositiveButton(R.string.apply, (dialog, which) -> {
                    submitRate(dialog);
                })
                .setNegativeButton(R.string.reject, (dialog, which) -> {
                    dialog.dismiss();
                }).create();
        TextView desc = view.findViewById(R.id.desc);
        String message = context.getResources().getString(R.string.rating_message, restaurantEntity.getTitle());
        desc.setText(message);
        alertDialog.show();

    }

    @SuppressLint("CheckResult")
    private void submitRate(final DialogInterface dialog) {
        DatabaseApi databaseApi = DatabaseApi.getInstance();

        databaseApi.findRestaurantByName(restaurantEntity.getTitle())
                .subscribeOn(Schedulers.io())
                .subscribe(entity -> {
                    entity.setRate(rate);
                    databaseApi.updateRestaurantRate(entity).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {
                                result.done();
                                Toast.makeText(context, R.string.rating_submited, Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }, this::onError);
                }, this::onError);

    }

    private void onError(Throwable throwable) {
        if (BuildConfig.DEBUG) {
            throwable.printStackTrace();
        }
    }
}
