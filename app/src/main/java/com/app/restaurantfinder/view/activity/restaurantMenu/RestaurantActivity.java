package com.app.restaurantfinder.view.activity.restaurantMenu;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.app.restaurantfinder.R;
import com.app.restaurantfinder.entity.food.FoodEntity;
import com.app.restaurantfinder.eventBus.FoodOrderEvent;
import com.app.restaurantfinder.eventBus.FoodRemoveEvent;
import com.app.restaurantfinder.model.init.FoodResponse;
import com.app.restaurantfinder.model.map.RestaurantModel;
import com.app.restaurantfinder.presenter.restaurantFood.FoodsPresenter;
import com.app.restaurantfinder.presenter.restaurantFood.FoodsPresenterImpl;
import com.app.restaurantfinder.util.CurrencyUtils;
import com.app.restaurantfinder.util.KeyHolder;
import com.app.restaurantfinder.view.activity.BaseActivity;
import com.app.restaurantfinder.view.dialog.money.MoneyFilterDialog;
import com.app.restaurantfinder.view.dialog.money.MoneyFilterResult;
import com.app.restaurantfinder.view.dialog.rate.RatingDialog;
import com.app.restaurantfinder.view.fragment.FoodMenuFragment;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestaurantActivity extends BaseActivity implements RestaurantView, MoneyFilterResult {

    @BindView(R.id.cover)
    ImageView cover;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.restaurant_title)
    TextView restaurantTitle;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.restaurant_rate)
    TextView restaurantRate;
    @BindView(R.id.foods_type)
    TabLayout foodsType;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.selectedFoods)
    TextView selectedFoods;

    @BindViews({R.id.back_shop, R.id.selectedFoods})
    List<View> shopViews;
    @BindView(R.id.back_shop)
    ImageView backShop;
    private FoodsPresenter presenter;
    private List<FoodEntity> selectedFoodEntities;
    private RestaurantModel restaurantModel;
    FoodResponse foodResponse;
    private MoneyFilterDialog moneyFilterDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        ButterKnife.bind(this);
        presenter = new FoodsPresenterImpl(this, this);
        getData();
        setVisibility(false);
        selectedFoodEntities = new ArrayList<>();
    }

    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(KeyHolder.RESTAURANT_VIEW_KEY)) {
                restaurantModel = extras.getParcelable(KeyHolder.RESTAURANT_VIEW_KEY);
                initView(restaurantModel);
            }
        }
    }

    private void initView(RestaurantModel restaurantModel) {
        Picasso.get().load(new File(restaurantModel.getImg())).into(cover);
        restaurantTitle.setText(restaurantModel.getTitle());
        String text = String.valueOf(restaurantModel.getRate());
        if (text.length() > 4) {
            text = text.substring(0, 4);
        }
        restaurantRate.setText(text);
        ratingBar.setRating(restaurantModel.getRate());
        presenter.getFoods(restaurantModel.getType());
    }

    private void initFoodTab(FoodResponse entities) {
        FoodTypeViewPager foodTypeViewPager = new FoodTypeViewPager(getSupportFragmentManager(), entities);
        viewPager.setAdapter(foodTypeViewPager);
        foodsType.setupWithViewPager(viewPager, false);
        viewPager.setCurrentItem(entities.getCategories().size() - 1);
    }

    @Override
    public void onFoods(FoodResponse foodEntities) {
        initFoodTab(foodEntities);
        this.foodResponse = foodEntities;

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFoodChoose(FoodOrderEvent orderEvent) {
        updateFactorView(orderEvent.getFoodEntity(), false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFoodRemove(FoodRemoveEvent orderEvent) {
        updateFactorView(orderEvent.getEntity(), true);
    }

    private void updateFactorView(FoodEntity foodEntity, boolean remove) {
        if (remove) {
            selectedFoodEntities.remove(foodEntity);
        } else {
            selectedFoodEntities.add(foodEntity);
        }
        selectedFoods.setText(String.valueOf(selectedFoodEntities.size()));
        setVisibility(selectedFoodEntities.size() != 0);

    }

    private void setVisibility(boolean visible) {
        for (View view : shopViews) {
            if (visible) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }
        }
    }

    int orderPrice = 0;

    @OnClick({R.id.back_shop, R.id.selectedFoods})
    public void factor() {
        Stream.of(selectedFoodEntities)
                .forEach(foodEntity -> orderPrice += Integer.parseInt(foodEntity.getPrice()));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.payment_title);

        builder.setMessage(getString(R.string.done, CurrencyUtils.makeMoney(orderPrice)));
        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            Toast.makeText(RestaurantActivity.this, R.string.done_order, Toast.LENGTH_SHORT).show();
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> {
            dialog.dismiss();
        });
        builder.create().show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @OnClick(R.id.rate)
    public void onRateClicked() {
        RatingDialog ratingDialog = new RatingDialog(this, restaurantModel, () -> {
            // response on rate
        });
        ratingDialog.show();
    }

    @OnClick(R.id.filter_money)
    public void onMoneyFilterClicked() {
        if (moneyFilterDialog == null) {
            moneyFilterDialog = new MoneyFilterDialog(this, this);
        }
        moneyFilterDialog.show();
    }

    private boolean rangeCheck(int money, int start, int end) {
        return money >= start && money <= end;
    }

    @Override
    public void onMoneyFilter(int start, int end) {
        if (foodResponse != null) {
            List<FoodEntity> foodEntities = Stream.of(foodResponse.getFoods())
                    .filter(value -> {
                        String priceValue = value.getPrice().replaceAll(",", "");
                        int price = Integer.parseInt(priceValue);
                        return rangeCheck(price, start, end);
                    }).toList();
            FoodResponse foodResponse = new FoodResponse();
            foodResponse.setCategories(this.foodResponse.getCategories());
            foodResponse.setFoods(foodEntities);
            initFoodTab(foodResponse);
        }

    }

    @Override
    public void removeFilter() {
        if (foodResponse != null) {
            initFoodTab(this.foodResponse);
        }
    }

    private class FoodTypeViewPager extends FragmentStatePagerAdapter {
        private final FoodResponse response;

        public FoodTypeViewPager(FragmentManager fm, FoodResponse response) {
            super(fm);
            this.response = response;
        }

        @Override
        public Fragment getItem(int position) {
            return FoodMenuFragment.getInstance(response.getCategories().get(getRtlPosition(position)), response.getFoods());
        }

        @Override
        public int getCount() {
            return response.getCategories().size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return response.getCategories().get(getRtlPosition(position)).getCategoryTitle();
        }

        private int getRtlPosition(int poistion) {
            return getCount() - poistion - 1;
        }
    }

}
