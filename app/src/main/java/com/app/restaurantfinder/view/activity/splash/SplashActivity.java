package com.app.restaurantfinder.view.activity.splash;

import androidx.appcompat.app.AppCompatActivity;
import lombok.val;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.util.SharedPreferenceUtils;
import com.app.restaurantfinder.view.activity.BaseActivity;
import com.app.restaurantfinder.view.activity.init.InitActivity;
import com.app.restaurantfinder.view.activity.login.LoginActivity;
import com.app.restaurantfinder.view.activity.main.MainActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler handler = new Handler();
        long WAIT_TIME = 2000;
        handler.postDelayed(() -> {
            Intent intent;
            if (SharedPreferenceUtils.isRegisteredUser(this)) {
                intent = new Intent(this, MainActivity.class);
            } else {
                if (SharedPreferenceUtils.isDatabaseCreated(this)) {
                    intent = new Intent(this, LoginActivity.class);
                } else {
                    intent = new Intent(this, InitActivity.class);
                }
            }
            startActivity(intent);
            finish();
        }, WAIT_TIME);
    }
}
