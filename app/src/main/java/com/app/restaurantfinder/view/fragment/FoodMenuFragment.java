package com.app.restaurantfinder.view.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;

import com.annimon.stream.Stream;
import com.app.restaurantfinder.R;
import com.app.restaurantfinder.adapter.FoodsAdapter;
import com.app.restaurantfinder.entity.food.FoodEntity;
import com.app.restaurantfinder.model.init.CategoryEntity;
import com.app.restaurantfinder.util.KeyHolder;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodMenuFragment extends Fragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.flipper)
    ViewFlipper flipper;

    private CategoryEntity categoryEntity = null;


    public FoodMenuFragment() {
        // Required empty public constructor
    }

    public static Fragment getInstance(CategoryEntity categoryEntity, List<FoodEntity> foods) {
        FoodMenuFragment foodMenuFragment = new FoodMenuFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelableArrayList(KeyHolder.RESTAURANT_FOOD_KEY, new ArrayList<>(foods));
        bundle.putParcelable(KeyHolder.RESTAURANT_FOOD_CATEGORY_KEY, categoryEntity);
        foodMenuFragment.setArguments(bundle);
        return foodMenuFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_food_menu, container, false);
        ButterKnife.bind(this, view);
        Bundle arguments = getArguments();
        ArrayList<FoodEntity> entities = null;
        if (arguments != null && arguments.containsKey(KeyHolder.RESTAURANT_FOOD_KEY)) {
            entities = arguments.getParcelableArrayList(KeyHolder.RESTAURANT_FOOD_KEY);
            categoryEntity = arguments.getParcelable(KeyHolder.RESTAURANT_FOOD_CATEGORY_KEY);
        }

        if (entities != null && categoryEntity != null) {
            List<FoodEntity> foodEntities = Stream.of(entities).filter(value -> (value.getCategoryId() == categoryEntity.getId()))
                    .toList();
            if (foodEntities != null && foodEntities.size() > 0) {
                flipper.setDisplayedChild(0);
                FoodsAdapter foodsAdapter = new FoodsAdapter(getContext(), foodEntities);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(foodsAdapter);
            } else {
                flipper.setDisplayedChild(1);
            }

        } else {
            flipper.setDisplayedChild(1);
        }

        return view;
    }

}
