package com.app.restaurantfinder.view.activity.map;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.R;
import com.app.restaurantfinder.model.map.RestaurantModel;
import com.app.restaurantfinder.util.KeyHolder;
import com.app.restaurantfinder.view.activity.BaseActivity;
import com.app.restaurantfinder.view.activity.restaurantMenu.RestaurantActivity;
import com.app.restaurantfinder.view.items.MapMarkerViewHolder;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.melnykov.fab.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import lombok.Getter;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.zoomIn)
    FloatingActionButton zoomIn;
    @BindView(R.id.zoomOut)
    FloatingActionButton zoomOut;
    @BindView(R.id.itemView)
    FrameLayout itemView;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Map<String, RestaurantModel> restaurantModelMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        initMap();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

    }

    private void initData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(KeyHolder.RESTAURANT_LIST_KEY)) {
                restaurantModelMap = new HashMap<>();
                ArrayList<RestaurantModel> items = extras.getParcelableArrayList(KeyHolder.RESTAURANT_LIST_KEY);
                for (RestaurantModel restaurantModel : items) {
                    restaurantModelMap.put(restaurantModel.getTitle(), restaurantModel);
                    addMarker(restaurantModel);
                }
                initMyLocation();
            } else if (extras.containsKey(KeyHolder.RESTAURANT_LOCATION_KEY)) {
                restaurantModelMap = new HashMap<>();
                RestaurantModel model = extras.getParcelable(KeyHolder.RESTAURANT_LOCATION_KEY);
                restaurantModelMap.put(model.getTitle(), model);
                addMarker(model);
            }

        }

    }

    private void initMyLocation() {

        getLocation().filter(location -> location != null).subscribe(location -> {
            initLocation(location);
        }, throwable -> {
            throwable.printStackTrace();
        }, () -> {
        });

    }

    @OnClick(R.id.myLoc)
    public void onViewClicked() {
        initMyLocation();
    }


    private Observable<Location> getLocation() {
        try {
            Task<Location> lastLocation = mFusedLocationProviderClient.getLastLocation();
            return Observable.fromPublisher(s -> {
                lastLocation.addOnCompleteListener(task -> {

                    s.onNext(task.getResult());
                });
            });
        } catch (SecurityException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            return Observable.empty();
        }
    }

    private void initLocation(Location location) {
        LatLng my = new LatLng(location.getLatitude(), location.getLongitude());
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(latLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,12f));
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(marker -> {
            Object tag = marker.getTag();
            if (tag instanceof String) {
                itemView.setVisibility(View.VISIBLE);
                itemView.removeAllViews();
                RestaurantModel model = restaurantModelMap.get(tag);
                itemView.addView(
                        makeRestaurantView(model));
            }
            return false;
        });
        mMap.setOnMapClickListener(latLng -> itemView.setVisibility(View.GONE));
        initData();

    }

    private View makeRestaurantView(RestaurantModel tag) {
        View inflate = LayoutInflater.from(this).inflate(R.layout.item_map_restaurant, null, false);
        RestaurantViewHolder restaurantViewHolder = new RestaurantViewHolder(inflate, tag);
        restaurantViewHolder.getOrder().setOnClickListener(v -> {
            String title = (String) v.getTag();
            RestaurantModel restaurantModel = restaurantModelMap.get(title);
            navigateToOrder(restaurantModel);

        });
        return inflate;
    }

    private void navigateToOrder(RestaurantModel restaurantModel) {
        Intent intent = new Intent(this, RestaurantActivity.class);
        Bundle bundle=new Bundle(1);
        bundle.putParcelable(KeyHolder.RESTAURANT_VIEW_KEY, restaurantModel);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private void addMarker(RestaurantModel restaurantModel) {
        MarkerOptions position = new MarkerOptions().position(restaurantModel.getLocation());
        MapMarkerViewHolder viewHolder = new MapMarkerViewHolder(this, restaurantModel);
        position.icon(BitmapDescriptorFactory.fromBitmap(viewHolder.getView()));
        Marker marker = mMap.addMarker(position);
        marker.setTag(restaurantModel.getTitle());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(restaurantModel.getLocation(), 14f));
    }

    @OnClick({R.id.zoomIn, R.id.zoomOut})
    public void onViewClicked(View view) {
        CameraPosition cameraPosition = mMap.getCameraPosition();
        float zoom = cameraPosition.zoom;
        if (view.getId() == R.id.zoomIn) {
            if (zoom + 1 <= mMap.getMaxZoomLevel()) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cameraPosition.target, zoom + 1));
            }
        } else if (view.getId() == R.id.zoomOut) {
            if (zoom - 1 >= mMap.getMinZoomLevel()) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cameraPosition.target, zoom - 1));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    static class RestaurantViewHolder {
        @BindView(R.id.restaurant_title)
        TextView restaurantTitle;
        @BindView(R.id.restaurant_rate)
        RatingBar restaurantRate;
        @BindView(R.id.restaurant_rate_value)
        TextView restaurantRateValue;
        @BindView(R.id.restaurant_style)
        TextView restaurantStyle;
        @BindView(R.id.restaurant_cover)
        ImageView restaurantCover;
        @Getter
        @BindView(R.id.order)
        TextView order;

        RestaurantViewHolder(View view, RestaurantModel restaurantModel) {
            ButterKnife.bind(this, view);
            setData(restaurantModel);
        }

        public void setData(RestaurantModel data) {
            restaurantTitle.setText(data.getTitle());
            restaurantRate.setRating(data.getRate());
            restaurantRateValue.setText(String.valueOf(data.getRate()));
            restaurantStyle.setText(data.getType().getName());
            Picasso.get().load(new File(data.getImg())).into(restaurantCover);
            order.setTag(data.getTitle());

        }

    }
}
