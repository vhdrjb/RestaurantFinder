package com.app.restaurantfinder.view.activity.init;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.ProgressBar;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.R;
import com.app.restaurantfinder.presenter.init.InitPresenter;
import com.app.restaurantfinder.presenter.init.InitPresenterImpl;
import com.app.restaurantfinder.view.activity.BaseActivity;
import com.app.restaurantfinder.view.activity.login.LoginActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.io.InputStream;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

public class InitActivity extends BaseActivity implements InitView {
    private final static int PERMISSION_LOCATION_CHECK_REQUEST = 101;
    private final static int PERMISSION_EXTERNAL_CHECK_REQUEST = 102;
    private final static int PERMISSION_ALL_CHECK_REQUEST = 100;
    private boolean locationPermission = false;
    private boolean externalPermission = false;
    @BindView(R.id.init_button)
    Button initButton;
    @BindView(R.id.progress)
    ProgressBar progress;
    private InitPresenter presenter;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        ButterKnife.bind(this);
        progress.setProgress(0);
        checkAllPermissions();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        presenter = new InitPresenterImpl(this, this);
        progress.setMax(InitPresenterImpl.MAX_SIZE);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("data/data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onSuccess() {
        Handler handler = new Handler();
        int waitTime =( progress.getMax() - progress.getProgress() )
                * 100;
        handler.postDelayed(() -> {
            for (int i = progress.getProgress(); i < progress.getMax(); i++) {
                progress.setProgress(i);
            }
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }, waitTime);
    }

    @Override
    public void onProgress() {
        progress.setProgress(progress.getProgress() + 1);
    }

    private void checkLocationPermissions() {
        locationPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!locationPermission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessage(R.string.location_permission_needed);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_CHECK_REQUEST);
            }
        }
    }

    private void checkExternalPermission() {
        externalPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (!externalPermission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showMessage(R.string.external_storage_needed);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_EXTERNAL_CHECK_REQUEST);
            }
        }
    }

    private void checkAllPermissions() {
        externalPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        locationPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!externalPermission && !locationPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_ALL_CHECK_REQUEST);
        }
    }

    @OnClick(R.id.init_button)
    public void onViewClicked() {
        if (locationPermission && externalPermission) {
            getLocation().subscribe(location -> {
                progress.setProgress(10);
                presenter.addData(location, loadJSONFromAsset());
            }, throwable -> {
                throwable.printStackTrace();
            }, () -> {
            });
        } else if (!locationPermission && !externalPermission) {
            checkAllPermissions();
        } else if (!locationPermission) {
            checkLocationPermissions();
        } else if (!externalPermission) {
            checkExternalPermission();
        }
    }

    private Observable<Location> getLocation() {
        try {
            Task<android.location.Location> lastLocation = mFusedLocationProviderClient.getLastLocation();
            return Observable.fromPublisher(s -> {
                lastLocation.addOnCompleteListener(task -> {

                    s.onNext(task.getResult());
                });
            });
        } catch (SecurityException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            return Observable.empty();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_ALL_CHECK_REQUEST) {
            externalPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        } else if (requestCode == PERMISSION_EXTERNAL_CHECK_REQUEST) {
            externalPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        } else if (requestCode == PERMISSION_LOCATION_CHECK_REQUEST) {
            locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        }

        if (locationPermission && externalPermission) {
            onViewClicked();
        }
    }
}
