package com.app.restaurantfinder.view.activity.main;

import com.app.restaurantfinder.entity.user.UserEntity;

public interface UserInfoView {
    void onUserData(UserEntity userEntity);

    void onWaitUserData();

    void onErrorUserData();

    void onLogOut();

    void redirectToLogin();
}
