package com.app.restaurantfinder.view.activity.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.annimon.stream.Stream;
import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.R;
import com.app.restaurantfinder.adapter.FoodMenuAdapter;
import com.app.restaurantfinder.adapter.RestaurantAdapter;
import com.app.restaurantfinder.entity.user.UserEntity;
import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.app.restaurantfinder.model.map.FoodMenuModel;
import com.app.restaurantfinder.model.map.RestaurantModel;
import com.app.restaurantfinder.presenter.init.InitPresenter;
import com.app.restaurantfinder.presenter.init.InitPresenterImpl;
import com.app.restaurantfinder.presenter.main.RestaurantPresenter;
import com.app.restaurantfinder.presenter.main.RestaurantPresenterImpl;
import com.app.restaurantfinder.presenter.userInfo.UserInfoPresenter;
import com.app.restaurantfinder.presenter.userInfo.UserInfoPresenterImpl;
import com.app.restaurantfinder.util.KeyHolder;
import com.app.restaurantfinder.view.activity.BaseActivity;
import com.app.restaurantfinder.view.activity.init.InitActivity;
import com.app.restaurantfinder.view.activity.init.InitView;
import com.app.restaurantfinder.view.activity.login.LoginActivity;
import com.app.restaurantfinder.view.activity.map.MapActivity;
import com.app.restaurantfinder.view.activity.register.RegisterActivity;
import com.app.restaurantfinder.view.activity.restaurantMenu.RestaurantActivity;
import com.app.restaurantfinder.view.activity.splash.SplashActivity;
import com.app.restaurantfinder.view.items.MapMarkerViewHolder;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

public class MainActivity extends BaseActivity implements OnMapReadyCallback, RestaurantView, UserInfoView {


    @BindView(R.id.menu_recycler)
    RecyclerView menuRecycler;
    @BindView(R.id.restaurant_recycler)
    RecyclerView restaurantRecycler;
    @BindView(R.id.searchField)
    EditText searchField;
    @BindView(R.id.user_info)
    TextView userInfo;
    @BindView(R.id.user_phone)
    TextView userPhone;
    @BindView(R.id.profile_layout)
    ConstraintLayout profileLayout;
    @BindView(R.id.viewFlipper)
    ViewFlipper viewFlipper;
    private GoogleMap mMap;
    private RestaurantPresenter restaurantPresenter;
    private RestaurantAdapter restaurantAdapter;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private final static int PERMISSION_LOCATION_CHECK_REQUEST = 101;
    private final static int PERMISSION_EXTERNAL_CHECK_REQUEST = 102;
    private final static int PERMISSION_ALL_CHECK_REQUEST = 100;
    private boolean locationPermission = false;
    private boolean externalPermission = false;

    private List<RestaurantModel> restaurantModels;
    private UserInfoPresenter infoPresenter;
    private UserEntity userEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        findViewById(R.id.main).requestFocus();
        restaurantPresenter = new RestaurantPresenterImpl(this, this);
        initMap();
        initFoodMenu();
        initRestaurantRecycler(new ArrayList<>());
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        initMyLocation();
        restaurantModels = new ArrayList<>();
        initSearchBar();
        infoPresenter = new UserInfoPresenterImpl(this, this);
        infoPresenter.getUserInfo();
        findViewById(R.id.myLoc).performClick();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initRestaurantRecycler(new ArrayList<>());
        restaurantPresenter.findAll();

    }

    private void initSearchBar() {
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (StringUtils.isNotBlank(s) && s.length() >= 3) {
                    List<RestaurantModel> models = restaurantAdapter.getModels();
                    List<RestaurantModel> restaurantModels = Stream.of(models).filter(value -> value.getTitle().contains(s)).toList();
                    initRestaurantRecycler(restaurantModels);
                } else {
                    initRestaurantRecycler(restaurantModels);
                }
            }
        });
    }

    private void initLocation(Location location) {
        LatLng my = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(my, 12f));
    }

    private void initRestaurantRecycler(List<RestaurantModel> restaurantModels) {
        restaurantAdapter = null;
        restaurantAdapter = new RestaurantAdapter(this, restaurantModels);
        restaurantRecycler.setAdapter(restaurantAdapter);
        restaurantRecycler.setLayoutManager(new LinearLayoutManager(this));
        restaurantAdapter.getRestaurantSelectedLocation().subscribe(this::restaurantLocationClicked, throwable -> {
            throwable.printStackTrace();
        }, () -> {
        });
        restaurantAdapter.getRestaurantOrder().subscribe(this::restaurantOrderClick, throwable -> {
            throwable.printStackTrace();
        }, () -> {
        });

    }

    private void restaurantOrderClick(RestaurantModel restaurantModel) {
        Intent intent = new Intent(this, RestaurantActivity.class);
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(KeyHolder.RESTAURANT_VIEW_KEY, restaurantModel);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void restaurantLocationClicked(RestaurantModel restaurantModel) {
        Intent intent = new Intent(this, MapActivity.class);
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(KeyHolder.RESTAURANT_LOCATION_KEY, restaurantModel);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void initFoodMenu() {
        FoodMenuModel all = new FoodMenuModel();
        all.setType(RestaurantStyle.ALL);

        FoodMenuModel cafe = new FoodMenuModel();
        cafe.setType(RestaurantStyle.CAFE);

        FoodMenuModel restarurant = new FoodMenuModel();
        restarurant.setType(RestaurantStyle.RESTAURANT);

        FoodMenuModel sandwich = new FoodMenuModel();
        sandwich.setType(RestaurantStyle.SANDWICH);

        FoodMenuModel nationalFood = new FoodMenuModel();
        nationalFood.setType(RestaurantStyle.NATIONAL_FOOD);

        FoodMenuModel pizza = new FoodMenuModel();
        pizza.setType(RestaurantStyle.PIZZA);

        FoodMenuModel fried = new FoodMenuModel();
        fried.setType(RestaurantStyle.FRIED);

        List<FoodMenuModel> foodMenuModels = new ArrayList<>();
        foodMenuModels.add(all);
        foodMenuModels.add(cafe);
        foodMenuModels.add(restarurant);
        foodMenuModels.add(sandwich);
        foodMenuModels.add(nationalFood);
        foodMenuModels.add(pizza);
        foodMenuModels.add(fried);
        FoodMenuAdapter foodMenuAdapter = new FoodMenuAdapter(this, foodMenuModels);
        menuRecycler.setAdapter(foodMenuAdapter);
        menuRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        foodMenuAdapter.getFoodTypeFilter().subscribe(this::onRestaurantFilter, throwable -> {
            throwable.printStackTrace();
        }, () -> {
        });
    }

    private void onRestaurantFilter(FoodMenuModel foodMenuModel) {
        searchField.setText("");
        if (foodMenuModel.getType().equals(RestaurantStyle.ALL)) {
            initRestaurantRecycler(restaurantModels);
        } else {
            List<RestaurantModel> typeRetaurant = Stream.of(restaurantModels)
                    .filter(value -> value.getType().equals(foodMenuModel.getType())).toList();
            initRestaurantRecycler(typeRetaurant);
        }

    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    public void onRestaurantFind(RestaurantModel restaurantModel) {
        restaurantAdapter.add(restaurantModel);
        restaurantModels.add(restaurantModel);
        addMarker(restaurantModel);
    }

    private void addMarker(RestaurantModel restaurantModel) {
        MarkerOptions position = new MarkerOptions().position(restaurantModel.getLocation());
        MapMarkerViewHolder viewHolder = new MapMarkerViewHolder(this, restaurantModel);
        position.icon(BitmapDescriptorFactory.fromBitmap(viewHolder.getView()));
        mMap.addMarker(position);
    }

    private void checkLocationPermissions() {
        locationPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!locationPermission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessage(R.string.location_permission_needed);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION_CHECK_REQUEST);
            }
        }
    }

    private void checkExternalPermission() {
        externalPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (!externalPermission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showMessage(R.string.external_storage_needed);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_EXTERNAL_CHECK_REQUEST);
            }
        }
    }

    private void checkAllPermissions() {
        externalPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        locationPermission = ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!externalPermission && !locationPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_ALL_CHECK_REQUEST);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_ALL_CHECK_REQUEST) {
            externalPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        } else if (requestCode == PERMISSION_EXTERNAL_CHECK_REQUEST) {
            externalPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        } else if (requestCode == PERMISSION_LOCATION_CHECK_REQUEST) {
            locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        }

        if (locationPermission && externalPermission) {
            onViewClicked();
        }
    }

    private Observable<Location> getLocation() {
        try {
            Task<Location> lastLocation = mFusedLocationProviderClient.getLastLocation();
            return Observable.fromPublisher(s -> {
                lastLocation.addOnCompleteListener(task -> {

                    s.onNext(task.getResult());
                });
            });
        } catch (SecurityException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            return Observable.empty();
        }
    }

    @OnClick(R.id.myLoc)
    public void onViewClicked() {
        initMyLocation();
    }

    private void initMyLocation() {
        if (locationPermission && externalPermission) {
            getLocation().filter(location -> location != null).subscribe(location -> {
                initLocation(location);
            }, throwable -> {
                throwable.printStackTrace();
            }, () -> {
            });
        } else if (!locationPermission && !externalPermission) {
            checkAllPermissions();
        } else if (!locationPermission) {
            checkLocationPermissions();
        } else if (!externalPermission) {
            checkExternalPermission();
        }
    }

    @OnClick(R.id.viewOnMap)
    public void viewOnMapClicked() {
        Intent intent = new Intent(this, MapActivity.class);
        Bundle bundle = new Bundle(1);
        bundle.putParcelableArrayList(KeyHolder.RESTAURANT_LIST_KEY, new ArrayList<>(restaurantAdapter.getModels()));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick(R.id.profile)
    public void onProfileClick(View view) {
        if (viewFlipper.getVisibility() == View.VISIBLE) {
            viewFlipper.setVisibility(View.GONE);
        } else if (viewFlipper.getVisibility() == View.GONE) {
            viewFlipper.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.log_out)
    public void onLogOutClicked() {
        if (userEntity != null) {
            infoPresenter.logOut();
        }

    }

    @Override
    public void onUserData(UserEntity userEntity) {
        viewFlipper.setDisplayedChild(0);
        userInfo.setText(userEntity.getFirstName() + userEntity.getLastName());
        userPhone.setText(userEntity.getPhoneNumber());
        this.userEntity = userEntity;
    }

    @Override
    public void onWaitUserData() {
        viewFlipper.setDisplayedChild(1);
    }

    @Override
    public void onErrorUserData() {
        Toast.makeText(this, R.string.user_info_exception_message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLogOut() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void redirectToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.restaurant_refresh)
    public void restaurantLocationRefresh() {
        Toast.makeText(this, R.string.refresh_restaurant_message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.exit_message)
                .setTitle(R.string.exit_title)
                .setPositiveButton(R.string.apply_exit
                        , (dialog, which) -> {
                    dialog.dismiss();
                    super.onBackPressed();
                })
                .setNegativeButton(R.string.reject_exit, (dialog, which) -> {
                    dialog.dismiss();
                })
                .create().show();
        ;

    }
}
