package com.app.restaurantfinder.view.activity.main;

import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;
import com.app.restaurantfinder.model.map.RestaurantModel;
import com.google.android.gms.maps.model.LatLng;

public interface RestaurantView {
    void onRestaurantFind(RestaurantModel restaurantModel);
}
