package com.app.restaurantfinder.view.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.presenter.login.LoginPresenter;
import com.app.restaurantfinder.presenter.login.LoginPresenterImpl;
import com.app.restaurantfinder.view.activity.BaseActivity;
import com.app.restaurantfinder.view.activity.main.MainActivity;
import com.app.restaurantfinder.view.activity.register.RegisterActivity;

import androidx.annotation.StringRes;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {

    @BindView(R.id.mobileField)
    EditText mobileField;
    @BindView(R.id.passwordField)
    EditText passwordField;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        this.presenter = new LoginPresenterImpl(this, this);
    }

    @OnClick(R.id.loginBtn)
    public void onLoginBtnClicked() {
        if (isValidData()) {
            this.presenter.authenticate(mobileField.getText().toString(),
                    passwordField.getText().toString());
        }
    }


    @OnClick(R.id.signUp)
    public void onSignUpClicked() {
        startActivity(RegisterActivity.class);
    }


    private boolean isValidData() {
        return mobileField.getText().length() == 11 &&
                passwordField.getText().length() >= 5;
    }

    @Override
    public void onSuccess() {
        startActivity(MainActivity.class);
    }

    @Override
    public void onFailure(@StringRes int message) {
        showMessage(message);

    }

    private void startActivity(Class activity) {
        Intent intent = new Intent(this, activity);
        super.startActivity(intent);
    }
}
