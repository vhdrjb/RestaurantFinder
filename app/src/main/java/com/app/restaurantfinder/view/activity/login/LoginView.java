package com.app.restaurantfinder.view.activity.login;


import androidx.annotation.StringRes;

public interface LoginView {

    void onSuccess();

    void onFailure(@StringRes int message);
}
