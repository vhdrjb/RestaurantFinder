package com.app.restaurantfinder.view.activity.register;

public interface RegisterView {
    void onRegister();

    void onFailure(int message);
}
