package com.app.restaurantfinder.view.items;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.app.restaurantfinder.model.map.RestaurantModel;
import com.app.restaurantfinder.util.ImageUitl;

import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MapMarkerViewHolder {
    private final Context context;
    private final RestaurantModel restaurantModel;
    @BindView(R.id.markerIconBack)
    ImageView markerIconBack;
    @BindView(R.id.markerIcon)
    ImageView markerIcon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    public Bitmap getView() {
        View view = LayoutInflater.from(context).inflate(R.layout.item_map_marker, null, false);
        ButterKnife.bind(this, view);
        markerIconBack.setImageDrawable(getBackground());
        markerIcon.setImageResource(ImageUitl.getImageResource(restaurantModel));
        title.setText(restaurantModel.getTitle());
        ratingBar.setRating(restaurantModel.getRate());
        return createDrawableFromView(context, view);
    }

    private Drawable getBackground() {
        String[] colors;
        if (restaurantModel.getType().equals(RestaurantStyle.CAFE)) {
            return ContextCompat.getDrawable(context, R.drawable.cafe_back_drawable);
        } else if (restaurantModel.getType().equals(RestaurantStyle.RESTAURANT)) {
            return ContextCompat.getDrawable(context, R.drawable.restaurant_back_drawable);
        } else if (restaurantModel.getType().equals(RestaurantStyle.SANDWICH)) {
            return ContextCompat.getDrawable(context, R.drawable.sandwich_back_drawable);
        } else if (restaurantModel.getType().equals(RestaurantStyle.NATIONAL_FOOD)) {
            return ContextCompat.getDrawable(context, R.drawable.national_food_back_drawable);
        } else if (restaurantModel.getType().equals(RestaurantStyle.PIZZA)) {
            return ContextCompat.getDrawable(context, R.drawable.pizza_back_drawable);
        } else {
            return ContextCompat.getDrawable(context, R.drawable.fried_back_drawable);
        }
    }

    private static Bitmap createDrawableFromView(Context context, View v) {
        v.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bitmap);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return bitmap;
    }


}
