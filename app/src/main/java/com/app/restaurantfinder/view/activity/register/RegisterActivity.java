package com.app.restaurantfinder.view.activity.register;

import android.os.Bundle;
import android.widget.EditText;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.entity.user.UserEntity;
import com.app.restaurantfinder.presenter.register.RegisterPresenter;
import com.app.restaurantfinder.presenter.register.RegisterPresenterImpl;
import com.app.restaurantfinder.view.activity.BaseActivity;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterView {

    @BindView(R.id.user_name)
    EditText userName;
    @BindView(R.id.user_last_name)
    EditText userLastName;
    @BindView(R.id.mobileField)
    EditText mobileField;
    @BindView(R.id.user_password)
    EditText userPassword;
    private RegisterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        presenter = new RegisterPresenterImpl(this, this);
    }

    @OnClick(R.id.register)
    public void onRegisterClicked() {
        if (isValidData()) {
            UserEntity userEntity = new UserEntity();
            userEntity.setFirstName(userName.getText().toString());
            userEntity.setLastName(userLastName.getText().toString());
            userEntity.setPhoneNumber(mobileField.getText().toString());
            String password = userPassword.getText().toString();
            presenter.register(userEntity, password);
        }else {
            showMessage(R.string.register_data_invalid_message);
        }
    }

    private boolean isValidData() {
        return StringUtils.isNotBlank(userName.getText().toString()) &&
                StringUtils.isNotBlank(userLastName.getText().toString()) &&
                mobileField.getText().length() == 11 &&
                userPassword.getText().length() >= 5;
    }

    @Override
    public void onRegister() {
        showMessage(R.string.user_registered_message);
        finish();
    }

    @Override
    public void onFailure(int message) {
        showMessage(message);
    }
}
