package com.app.restaurantfinder.deserializer;

import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Random;


public class RestaurantDeserializer implements JsonDeserializer<RestaurantStyle> {
    @Override
    public RestaurantStyle deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonArray asJsonArray = jsonElement.getAsJsonArray();
        int size = asJsonArray.size();
        Random random = new Random();
        int index = random.nextInt(size);
        JsonObject jsonObject = asJsonArray.get(index).getAsJsonObject();
        int id = jsonObject.get("id").getAsInt();
        if (id == 1) {
            return RestaurantStyle.RESTAURANT;
        } else if (id == 3) {
            return RestaurantStyle.SANDWICH;
        } else if (id == 5) {
            return RestaurantStyle.NATIONAL_FOOD;
        } else if (id == 7) {
            return RestaurantStyle.PIZZA;
        } else if (id == 9) {
            return RestaurantStyle.CAFE;
        } else if (id == 10) {
            return RestaurantStyle.FRIED;
        }
        return null;
    }
}
