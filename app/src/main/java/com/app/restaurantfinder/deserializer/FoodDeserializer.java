package com.app.restaurantfinder.deserializer;

import com.app.restaurantfinder.entity.food.FoodType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class FoodDeserializer implements JsonDeserializer<FoodType> {
    @Override
    public FoodType deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        int asInt = jsonElement.getAsInt();
        for (FoodType foodType : FoodType.values()) {
            if (foodType.getId() == asInt) {
                return foodType;
            }
        }
        return null;
    }
}
