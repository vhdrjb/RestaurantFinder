package com.app.restaurantfinder.db;

import android.content.Context;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;
import com.app.restaurantfinder.entity.user.UserEntity;

import java.util.List;

import androidx.room.Room;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class DatabaseApi {
    private static DatabaseApi INSTANCE;
    private DatabaseCore databaseCore;
    private final static String DATABASE_NAME = BuildConfig.APPLICATION_ID + "_database";

    public static DatabaseApi getInstance() {
        return INSTANCE;
    }

    public static void init(Context context) {
        INSTANCE = new DatabaseApi(context);
    }

    private DatabaseApi(Context context) {
        databaseCore = Room.databaseBuilder(context, DatabaseCore.class, DATABASE_NAME).build();
    }

    public Single<Long> insertUser(UserEntity userEntity) {
        return databaseCore.userDao().insertUser(userEntity);
    }

    public Completable insertRestaurant(RestaurantEntity restaurantEntity) {
        return databaseCore.restaurantDao().insert(restaurantEntity);
    }

    public Observable<List<RestaurantEntity>> findAllRestaurant() {
        return databaseCore.restaurantDao().findAll();
    }

    public Single<UserEntity> findById(long id) {
        return databaseCore.userDao().findById(id);
    }

    public Completable deleteUser(UserEntity userEntity) {
        return databaseCore.userDao().deleteUser(userEntity);
    }

    public Completable updateRestaurantRate(RestaurantEntity restaurantEntity) {
        return databaseCore.restaurantDao().update(restaurantEntity);
    }

    public Single<RestaurantEntity> findRestaurantByName(String name) {
        return databaseCore.restaurantDao().findByName(name);
    }

    public Completable resetRestaurants() {
        return databaseCore.restaurantDao().delete();
    }
}
