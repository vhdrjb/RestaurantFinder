package com.app.restaurantfinder.db;

import com.app.restaurantfinder.BuildConfig;
import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;
import com.app.restaurantfinder.entity.user.UserEntity;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {UserEntity.class,RestaurantEntity.class}, version = BuildConfig.VERSION_CODE)
public abstract class DatabaseCore extends RoomDatabase {
    public abstract UserDao userDao();

    public abstract RestaurantDao restaurantDao();
}
