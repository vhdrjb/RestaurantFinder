package com.app.restaurantfinder.db;

import com.app.restaurantfinder.entity.user.UserEntity;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface UserDao {
    @Query("select * from userentity where id = :id")
    Single<UserEntity> findById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insertUser(UserEntity userEntity);

    @Delete
    Completable deleteUser(UserEntity userEntity);

}
