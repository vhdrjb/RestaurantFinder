package com.app.restaurantfinder.db;

import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

@Dao
public interface RestaurantDao {
    @Query("select * from restaurantentity")
    Observable<List<RestaurantEntity>> findAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(RestaurantEntity restaurantEntity);

    @Update
    Completable update(RestaurantEntity restaurantEntity);

    @Query("select * from restaurantentity where restaurantName =:restaurantName")
    Single<RestaurantEntity> findByName(String restaurantName);

    @Query("delete from RestaurantEntity")
    Completable delete();
}
