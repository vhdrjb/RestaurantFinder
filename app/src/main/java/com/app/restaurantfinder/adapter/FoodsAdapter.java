package com.app.restaurantfinder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.entity.food.FoodEntity;
import com.app.restaurantfinder.eventBus.FoodOrderEvent;
import com.app.restaurantfinder.eventBus.FoodRemoveEvent;
import com.app.restaurantfinder.util.CurrencyUtils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FoodsAdapter extends RecyclerView.Adapter<FoodsAdapter.FoodViewHolder> {
    private final Context context;
    private final List<FoodEntity> entities;

    @NonNull
    @Override
    public FoodsAdapter.FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_food_order, parent, false);
        return new FoodViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodsAdapter.FoodViewHolder holder, int position) {
        holder.setData(entities.get(position));
    }

    private void updateModel(FoodEntity foodEntity) {
        entities.set(entities.indexOf(foodEntity), foodEntity);

    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    class FoodViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.foodTitle)
        TextView foodTitle;
        @BindView(R.id.foodPrice)
        TextView foodPrice;
        @BindView(R.id.foodDesc)
        TextView foodDesc;
        @BindView(R.id.count)
        TextView count;
        @BindView(R.id.remove)
        ImageView remove;
        private int counter = 0;
        private FoodEntity data;
        @BindView(R.id.food_cover)
        ImageView foodCover;
        private final Context context;

        FoodViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);

        }

        public void setData(FoodEntity data) {
            this.foodTitle.setText(data.getFoodName());
            this.foodPrice.setText(CurrencyUtils.makeMoney(data.getPrice()));
            this.foodDesc.setText(data.getDescription());
            String img = context.getExternalFilesDir("foods").getAbsolutePath() + File.separator + data.getId() + ".jpg";
            Picasso.get().load(new File(img)).into(foodCover);
            this.data = data;
            counter = data.getCount();
            refreshView();
        }


        @OnClick({R.id.remove, R.id.add})
        public void changeCount(View view) {
            if (view.getId() == R.id.remove) {
                if (counter > 0) {
                    counter--;
                    EventBus.getDefault().post(new FoodRemoveEvent(data));
                }
            } else if (view.getId() == R.id.add) {
                counter++;
                EventBus.getDefault().post(new FoodOrderEvent(data));
            }
            refreshView();
        }

        private void refreshView() {
            data.setCount(counter);
            updateModel(data);
            if (counter == 0) {
                remove.setVisibility(View.INVISIBLE);
                count.setVisibility(View.INVISIBLE);
            } else {
                remove.setVisibility(View.VISIBLE);
                count.setVisibility(View.VISIBLE);
                count.setText(String.valueOf(counter));
            }
        }
    }
}
