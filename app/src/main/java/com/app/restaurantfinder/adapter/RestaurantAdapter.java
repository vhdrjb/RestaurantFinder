package com.app.restaurantfinder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.model.map.RestaurantModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.subjects.PublishSubject;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder> {
    private final Context context;
    @Getter
    private final List<RestaurantModel> models;
    @Getter
    private final PublishSubject<RestaurantModel> restaurantSelectedLocation;
    @Getter
    private final PublishSubject<RestaurantModel> restaurantOrder;
    public RestaurantAdapter(Context context, List<RestaurantModel> models) {
        this.context = context;
        this.models = models;
        restaurantSelectedLocation = PublishSubject.create();
        restaurantOrder = PublishSubject.create();
    }

    @NonNull
    @Override
    public RestaurantAdapter.RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_restaurant, viewGroup, false);
        return new RestaurantViewHolder(view, restaurantSelectedLocation, restaurantOrder);
    }

    public void add(RestaurantModel restaurantModel) {
        models.add(restaurantModel);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantAdapter.RestaurantViewHolder viewHolder, int i) {
        viewHolder.setData(models.get(i));
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    static class RestaurantViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.resturant_location)
        ImageView resturantLocation;
        @BindView(R.id.restaurant_title)
        TextView restaurantTitle;
        @BindView(R.id.restaurant_rate)
        RatingBar restaurantRate;
        @BindView(R.id.restaurant_rate_value)
        TextView restaurantRateValue;
        @BindView(R.id.restaurant_style)
        TextView restaurantStyle;
        @BindView(R.id.restaurant_cover)
        ImageView restaurantCover;
        private final PublishSubject<RestaurantModel> locationPublisher;
        private final PublishSubject<RestaurantModel> restaurantOrder;
        private View view;
        RestaurantViewHolder(View view, PublishSubject locationPublisher,PublishSubject restaurantOrder) {
            super(view);
            ButterKnife.bind(this, view);
            this.locationPublisher = locationPublisher;
            this.restaurantOrder = restaurantOrder;
            this.view = view;
        }

        public void setData(RestaurantModel data) {
            restaurantTitle.setText(data.getTitle());
            restaurantRate.setRating(data.getRate());
            restaurantRateValue.setText(String.valueOf(data.getRate()));
            if (data.getType() == null) {
                System.out.println(data);
            }
            restaurantStyle.setText(data.getType().getName());
            Picasso.get().load(new File(data.getImg())).into(restaurantCover);
            resturantLocation.setOnClickListener(v -> locationPublisher.onNext(data));
            this.view.setOnClickListener(v -> restaurantOrder.onNext(data));
        }


    }

}
