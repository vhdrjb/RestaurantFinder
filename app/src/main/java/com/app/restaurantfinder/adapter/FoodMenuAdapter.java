package com.app.restaurantfinder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.model.map.FoodMenuModel;
import com.app.restaurantfinder.util.GradientUtils;
import com.app.restaurantfinder.util.ImageUitl;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class FoodMenuAdapter extends RecyclerView.Adapter<FoodMenuAdapter.FoodMenuViewHolder> {
    @NonNull
    private final Context context;
    @NonNull
    private final List<FoodMenuModel> models;
    @Getter
    private final PublishSubject<FoodMenuModel> foodTypeFilter;
    public FoodMenuAdapter(@NonNull Context context, @NonNull List<FoodMenuModel> models) {
        this.context = context;
        this.models = models;
        foodTypeFilter = PublishSubject.create();
    }

    @NonNull
    @Override
    public FoodMenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_food_menu, viewGroup, false);
        return new FoodMenuViewHolder(context, view,foodTypeFilter);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodMenuViewHolder foodMenuViewHolder, int i) {
        foodMenuViewHolder.setData(models.get(i));

    }

    @Override
    public int getItemCount() {
        return models.size();
    }



    static class FoodMenuViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.food_menu_icon)
        ImageView foodMenuIcon;
        @BindView(R.id.food_menu_title)
        TextView foodMenuTitle;
        @BindView(R.id.food_menu_subtitle)
        TextView foodMenuSubtitle;
        @BindView(R.id.layout)
        ConstraintLayout card;
        private final Context context;
        private final PublishSubject<FoodMenuModel> foodPublisher;
        FoodMenuViewHolder(Context context, View view, PublishSubject<FoodMenuModel> foodTypeFilter) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
            this.foodPublisher = foodTypeFilter;
        }

        public void setData(FoodMenuModel data) {
            foodMenuIcon.setImageResource(ImageUitl.getImageResource(data));
            card.setBackground(ContextCompat.getDrawable(context, GradientUtils.getBackground(data)));
            foodMenuTitle.setText(data.getType().getName());
            foodMenuSubtitle.setText(data.getType().getDescription());
            card.setOnClickListener(v -> foodPublisher.onNext(data));
        }
    }
}
