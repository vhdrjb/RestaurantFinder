package com.app.restaurantfinder.entity.restaurant;

import com.app.restaurantfinder.entity.convertor.RestaurantTypeConvertor;
import com.app.restaurantfinder.model.enums.RestaurantStyle;

import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity()
@Getter
@Setter
@NoArgsConstructor
public class RestaurantEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String restaurantName;
    private float rate;
    @Embedded()
    private LocationModel locationModel;
    @TypeConverters(RestaurantTypeConvertor.class)
    private RestaurantStyle type;
    private String img;
}

