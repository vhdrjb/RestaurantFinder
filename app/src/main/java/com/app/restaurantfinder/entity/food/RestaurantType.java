package com.app.restaurantfinder.entity.food;

import com.app.restaurantfinder.R;

public enum  RestaurantType {
    CAFE,RESTAURANT,
    DRINK,SANDWICH,
    NATIONAL_FOOD,PIZZA,
    FRIED
}
