package com.app.restaurantfinder.entity.food;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum FoodType {
    PIZZA(28523,5),BURGUR(28520,4),SANDWICH(28522,3),FRIED(28521,2),B_MEAL(28519,1),DRINK(28518,0);

    @Getter
    private int id;
    @Getter
    private int index;
}
