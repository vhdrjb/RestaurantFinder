package com.app.restaurantfinder.entity.food;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.restaurantfinder.deserializer.FoodDeserializer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(exclude = {"description", "price"})
public class FoodEntity implements Parcelable {
    @SerializedName("price")
    private String price;
    @SerializedName("name")
    private String foodName;
    @SerializedName("description")
    private String description;
    @SerializedName("category_id")
    private long categoryId;
    @SerializedName("id")
    private String id;

    @Expose(serialize = false,deserialize = false)
    private int count;

    protected FoodEntity(Parcel in) {
        price = in.readString();
        foodName = in.readString();
        description = in.readString();
        categoryId = in.readLong();
        id = in.readString();
    }

    public static final Creator<FoodEntity> CREATOR = new Creator<FoodEntity>() {
        @Override
        public FoodEntity createFromParcel(Parcel in) {
            return new FoodEntity(in);
        }

        @Override
        public FoodEntity[] newArray(int size) {
            return new FoodEntity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(price);
        dest.writeString(foodName);
        dest.writeString(description);
        dest.writeLong(categoryId);
        dest.writeString(id);
    }
}
