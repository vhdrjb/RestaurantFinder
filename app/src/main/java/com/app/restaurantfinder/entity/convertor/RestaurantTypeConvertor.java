package com.app.restaurantfinder.entity.convertor;

import com.app.restaurantfinder.model.enums.RestaurantStyle;

import androidx.room.TypeConverter;

public class RestaurantTypeConvertor {
    @TypeConverter
    public static RestaurantStyle toStyle(int value) {
        if (value == 0) {
            return RestaurantStyle.CAFE;
        } else if (value == 2) {
            return RestaurantStyle.FRIED;
        } else if (value == 3) {
            return RestaurantStyle.NATIONAL_FOOD;
        } else if (value == 4) {
            return RestaurantStyle.PIZZA;
        } else if (value == 5) {
            return RestaurantStyle.RESTAURANT;
        } else if (value == 6) {
            return RestaurantStyle.SANDWICH;
        }else{
            return null;
        }
    }

    @TypeConverter
    public static int toInt(RestaurantStyle style) {
        if (style.equals(RestaurantStyle.CAFE)) {
            return 0;
        } else if (style.equals(RestaurantStyle.FRIED)) {
            return 2;
        } else if (style.equals(RestaurantStyle.NATIONAL_FOOD)) {
            return 3;
        } else if (style.equals(RestaurantStyle.PIZZA)) {
            return 4;
        } else if (style.equals(RestaurantStyle.RESTAURANT)) {
            return 5;
        } else if (style.equals(RestaurantStyle.SANDWICH)) {
            return 6;
        }
        return -1;
    }
}
