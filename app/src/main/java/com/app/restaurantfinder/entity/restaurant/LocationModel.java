package com.app.restaurantfinder.entity.restaurant;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class LocationModel {
    private Double latitude;
    private Double longitude;
}
