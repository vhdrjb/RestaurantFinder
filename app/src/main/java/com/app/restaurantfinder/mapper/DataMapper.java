package com.app.restaurantfinder.mapper;

import com.app.restaurantfinder.entity.restaurant.LocationModel;
import com.app.restaurantfinder.entity.restaurant.RestaurantEntity;
import com.app.restaurantfinder.model.init.RestaurantResponseModel;

public class DataMapper {
    public RestaurantEntity map(RestaurantResponseModel model, String img, LocationModel locationModel) {
        RestaurantEntity entity = new RestaurantEntity();
        entity.setType(model.getType());
        entity.setRestaurantName(model.getRestaurantName());
        entity.setRate(model.getRank());
        entity.setImg(img);
        entity.setLocationModel(locationModel);
        return entity;
    }
}
