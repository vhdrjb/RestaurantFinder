package com.app.restaurantfinder.model.init;

import com.app.restaurantfinder.deserializer.RestaurantDeserializer;
import com.app.restaurantfinder.model.enums.RestaurantStyle;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class RestaurantResponseModel {
    @SerializedName("name")
    private String restaurantName;
    @SerializedName("rank")
    private float rank;
    @SerializedName("cuisine")
    @JsonAdapter(RestaurantDeserializer.class)
    private RestaurantStyle type;
}
