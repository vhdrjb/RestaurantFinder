package com.app.restaurantfinder.model.enums;

import com.app.restaurantfinder.R;
import com.app.restaurantfinder.entity.convertor.RestaurantTypeConvertor;

import androidx.room.TypeConverters;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum RestaurantStyle {
    ALL(R.string.all_type,R.string.all_type_desc),CAFE(R.string.cafe_title,R.string.cafe_desc),
    RESTAURANT(R.string.restaurant_title,R.string.restaurant_desc),
    SANDWICH(R.string.sandwich_title,R.string.sandwich_desc),
    NATIONAL_FOOD(R.string.national_food_title,R.string.national_food_desc),PIZZA(R.string.pizaa_title,R.string.pizza_desc),
    FRIED(R.string.fried,R.string.fried_type_desc);

    @Getter
    private int name;
    @Getter
    private int description;
}
