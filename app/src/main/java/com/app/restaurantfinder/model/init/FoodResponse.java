package com.app.restaurantfinder.model.init;

import com.app.restaurantfinder.entity.food.FoodEntity;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FoodResponse {
    @SerializedName("food")
    List<FoodEntity> foods;
    @SerializedName("categories")
    List<CategoryEntity> categories;
}
