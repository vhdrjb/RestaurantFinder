package com.app.restaurantfinder.model.init;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryEntity implements Parcelable {
    @SerializedName("name")
    private String categoryTitle;
    @SerializedName("id")
    private long id;

    protected CategoryEntity(Parcel in) {
        categoryTitle = in.readString();
        id = in.readLong();
    }

    public static final Creator<CategoryEntity> CREATOR = new Creator<CategoryEntity>() {
        @Override
        public CategoryEntity createFromParcel(Parcel in) {
            return new CategoryEntity(in);
        }

        @Override
        public CategoryEntity[] newArray(int size) {
            return new CategoryEntity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryTitle);
        dest.writeLong(id);
    }
}
