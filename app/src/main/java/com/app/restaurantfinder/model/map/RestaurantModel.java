package com.app.restaurantfinder.model.map;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class RestaurantModel extends FoodModel implements Parcelable {
    private String title;
    private float rate;
    private LatLng location;
    private String img;

    protected RestaurantModel(Parcel in) {
        super(in);
        title = in.readString();
        rate = in.readFloat();
        location = in.readParcelable(LatLng.class.getClassLoader());
        img = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(title);
        dest.writeFloat(rate);
        dest.writeParcelable(location, flags);
        dest.writeString(img);
    }

    public static final Creator<RestaurantModel> CREATOR = new Creator<RestaurantModel>() {
        @Override
        public RestaurantModel createFromParcel(Parcel in) {
            return new RestaurantModel(in);
        }

        @Override
        public RestaurantModel[] newArray(int size) {
            return new RestaurantModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}
