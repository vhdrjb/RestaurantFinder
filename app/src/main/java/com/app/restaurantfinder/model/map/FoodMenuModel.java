package com.app.restaurantfinder.model.map;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.restaurantfinder.model.enums.RestaurantStyle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class FoodMenuModel extends FoodModel implements Parcelable {

    protected FoodMenuModel(Parcel in) {
        super(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    public static final Creator<FoodMenuModel> CREATOR = new Creator<FoodMenuModel>() {
        @Override
        public FoodMenuModel createFromParcel(Parcel in) {
            return new FoodMenuModel(in);
        }

        @Override
        public FoodMenuModel[] newArray(int size) {
            return new FoodMenuModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}
