package com.app.restaurantfinder.model.map;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.restaurantfinder.model.enums.RestaurantStyle;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class FoodModel implements Parcelable {
    private RestaurantStyle type;

    public FoodModel() {
    }

    protected FoodModel(Parcel in) {
        type = RestaurantStyle.valueOf(in.readString());
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type.name());
    }
}
