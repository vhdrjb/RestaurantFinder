package com.app.restaurantfinder.eventBus;

import com.app.restaurantfinder.entity.food.FoodEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FoodRemoveEvent {
    private FoodEntity entity;
}
